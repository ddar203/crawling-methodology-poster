\section{A Comparative Framework} \label{sec:methodology}

% \begin{figure}[htb]
% \centering
%     \includegraphics[width=9cm,height=13cm]{figures/s4.png}
%     \caption{System architecture.}
%     \label{fig:system}
% \end{figure}


In this section, we describe our framework for empirically highlighting the
differences in the network traffic generated between the crawlers described in
\Cref{sec:evaluation}.
%
% In an effort to guide researchers, we also release an open
% framework for crawler comparison and benchmarking 
% at \publicreleaseurl along with documentation
% for extending it to enable comparisons with crawlers not included in our study.
\Cref{tab:versions} shows the version information of crawlers used in this
framework. Our evaluation framework focuses on uncovering the impact of
crawlers on requests generated and responses received by clients and traffic
characteristics observed by the network.
% \begin{itemize}[leftmargin=*]
%   \item \emph{Client generated data.} How different are the requests generated
%     by the client running different crawlers? 
%   \item \emph{Server generated data.} How different are the resulting responses
%     received from the destination web servers?
%   \item \emph{Network observable data.} How different is data observed by
%     network-level devices? 
% \end{itemize}
% 
Our hope is that this framework will enable more complete, sound, 
and correct research
which considers and reports the impact of different crawling methodologies on
their hypotheses.

% \input{tables/versions.tex}

% \subsection{Framework architecture}\label{sec:methodology:architecture}
% 
% Our framework relies on a master-worker architecture. \Cref{fig:system} illustrates the system
% architecture. 
% Here, the master
% coordinates the crawl which occurs through multiple workers. The master program
% is given the task of assigning IP addresses, checking the IP
% reputations, and assigning domains to be crawled to each worker.
% Each worker is essentially a machine which runs a program which wraps around
% a crawling tool. This program takes a domain name as input and instructs the
% crawling tool to crawl it. Below, we explain the interactions between the
% master and worker in more detail.

% \para{Initializing master and workers.} During the initialization phase, the
% workers send a ``\emph{start}'' message to the master. The master responds with
% a ``\emph{bind} \texttt{<ip>}'' message. The workers dynamically bind this IP
% address to their crawling interfaces. Once this binding is done, each worker
% sends a ``\emph{ready}'' message to the master.

% \para{Master-to-worker instructions.} Once the master has received
% a ``\emph{ready}'' (or possibly ``\emph{error}'') message from all the workers,
% the master performs two tasks: (1) checks that the reputations of all IP
% addresses assigned to the workers are similar and (2) instruct the workers to
% crawl a specific domain.

% \parai{IP reputation checking.} The master performs an IP reputation
% check to ensure that all workers have IP addresses that are not associated
% with malicious activity and that all workers have consistent IP reputations. 
% This is done using Cisco's Talos public API \cite{Talos-Web}.
% In the event that an IP address has a poor reputation, it is swapped for
% one which has a reputation similar to the other workers. This swap is
% accomplished by sending a new ``\emph{bind} \texttt{<ip>}'' message.

% \parai{Crawling. } After all the workers are known to have IP
% addresses with similar reputations, the master sends each one of them
% a ``\emph{crawl} \texttt{<domain>}'' command. This causes the workers to
% invoke their local program to crawl the specified domain.

% \para{Worker-to-master notifications.} Workers can communicate to the master
% using three messages: ``\emph{start}'', ``\emph{ready}'', and ``\emph{error}''.
% The ``\emph{start}'' message is used during initialization to signal to the
% master that the worker is ready to bind an IP address. The ``\emph{ready}''
% message is the default response to any instruction from the master that was
% successfully completed (\eg \emph{bind} or \emph{crawl}). Conversely, the
% ``\emph{error}'' message is the default response to any failed instruction from
% the master. These are logged by the workers and the master.

% \para{Data collection.} Each worker in our framework automatically launches
% \texttt{tcpdump} during the period between the arrival of a ``\emph{crawl}''
% instruction and dispatch of a ``\emph{ready}'' message. This allows us to
% analyze characteristics of network-flows and (when HTTPS is not used) HTTP
% requests and responses. We note that this data collection can occur in addition
% to the data collected by the crawling tools themselves (\eg screenshots, HTTP
% archive files, \etc).


\subsection{Design considerations} \label{sec:methodology:design}

Our framework was designed with two specific goals: extensibility and
correctness. To achieve high extensibility, we seek to make our framework
modular and easily configurable. Achieving correctness is more challenging due
to a number of confounding factors when performing comparative crawls,
including: (1) websites are frequently updated and crawls starting at different
times may observe different content, (2) web servers may provide localized
results making it challenging to rely on cloud infrastructure for measurements,
and (3) websites might trigger IP address based discrimination against IPs
suspected to be associated with crawlers. To achieve extensibility and address
the above confounding factors which impact correctness, we take the following
steps:

\para{Rely on an easily configurable master-worker architecture.} 
The current implementation of the master only requires three configuration
parameters: a list of worker machines, available IP addresses from which the
crawls are to be conducted, and target domains to crawl. 
Each worker machine only needs to implement a wrapper program which takes
as input a domain name. It then crawls this domain using any crawling tool. Our
first release includes wrappers for the eight different crawlers. Workers only
require two configuration parameters: the master's IP address and the
command line instruction to launch the local wrapper program.

\para{Perform crawl synchronization.}
We make an effort to achieve a reasonable level of synchronization between
crawls from different tools. Our master script coordinates efforts in such
a way that all workers begin crawling a specific webpage at approximately
the same time (granularity of seconds). This allows us to negate the impact
of frequent website updates within a small margin of error.

\para{Account for changing IP reputations.} 
To account for web servers discriminating our workers based on blacklisting and 
IP reputation scores, we
integrate Cisco's public Talos Intelligence in our master program. Talos returns
one of three classes for each queried IP address -- `Good', `Neutral', or `Poor'.
If any worker is found to have an IP reputation that is lower than the
others, a new IP address with similar reputation to the IPs used by other
workers is dynamically reassigned to it.
Therefore, each worker is conducting its crawl from an IP address of
comparable reputation to the other worker IP addresses. A limitation of our
approach is that we rely on intelligence data from a single source --
Talos \cite{Talos-Web}. However, our IP intelligence class can be easily
extended to use APIs from other intelligence services.


\subsection{Deployment for this study} \label{sec:methodology:deployment}

% For the purpose of our evaluation in \Cref{sec:comparative}, our framework was
% configured as follows.
% 
\para{Master and worker machine setup.} We used one virtual machine for
each worker and master -- each with a unique IP address. In total, we had eight
workers -- one for each crawler. Each
worker was configured to use its crawling tool in its default
configuration, with the exception of OpenWPM for which we also enabled its
``anti-bot detection'' functions. 

\para{IP addresses.} We obtained 22 unique IPv4 addresses which were
used in our crawl. These IPv4 addresses were obtained from a regional ISP
for the duration of these experiments and were found to have a ``Good'' IP
reputation at the start of our measurements. Our IP addresses were
geolocated to the same region thereby reducing the impact of content
localization.

\para{URL list.} Our URLs included the union of the Top 500
domains from Alexa \cite{Alexa-Web} and Umbrella's \cite{Umbrella-Web}
top sites lists. In total, there were 932 unique domains (of which 30
domains were removed as they returned SOA records) 
which were crawled by each of our workers. The domain lists were
fetched on {25$^{th}$ April 2019}. %A snapshot of the specific URLs generated by
%this union is available at \publicreleaseurl.
 
